## Alekenov test task

Бэкенд принимающий Телефон, почту, сообщение с валидацией
- /api/form

Бэкенд возвращающии информацию о местоположении по ip текущего пользователя

- /api/ip

## Установка (nginx, apache)

- Скачать в окружающую среду: git clone https://gitlab.com/Sarsebek/alekenov.git
- cd Alekenov
- touch .env || cp .env.example .env
- Записать свои конфигурации подключения к бд
- composer install
- php artisan key:generate && php artisan optimize && php artisan config:clear && php artisan migrate

## Установка (docker)

- Скачать в окружающую среду: git clone https://gitlab.com/Sarsebek/alekenov.git
- cd Alekenov
- touch .env || cp .env.example .env
- docker-compose build app
- docker-compose up -d
- docker-compose exec app composer install
- docker-compose exec app php artisan key:generate

## Документация

- в главной странице

<img src ="https://gitlab.com/Sarsebek/alekenov/-/raw/main/public/doc.png" >
