<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 * @OA\Property(property="email", type="string", readOnly="true"),
 * @OA\Property(property="phone", type="string", readOnly="true"),
 * @OA\Property(property="message", type="string", readOnly="true"),
 * )
 * Class Form
 *
 */
class Form extends Model
{


}
