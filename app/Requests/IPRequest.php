<?php


namespace App\Requests;


use ApiipClient\Apiip;

class IPRequest    extends BaseRequest
{
    public function info(\Illuminate\Http\Request $request){

        $access_key = env('APIIP_TOKEN', '69e21da1-f3ad-44ee-84ee-2e620c215383');
        $client = new Apiip($access_key);
        $details = $client->getLocation();

        // Decode JSON response
        return $details;

    }

}