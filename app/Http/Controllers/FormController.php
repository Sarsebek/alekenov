<?php

namespace App\Http\Controllers;

use ApiipClient\Apiip;
use App\Requests\IPRequest;
use App\Rules\Phone;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class FormController extends Controller
{
    /**
     * @OA\Post(
     * path="/api/form",
     * summary="Form",
     * description="Form for test task",
     * operationId="form",
     * tags={"Form"},
     * @OA\RequestBody(
     *    required=true,
     *    description="Pass data",
     *    @OA\JsonContent(
     *       required={"email","phone", "message"},
     *       @OA\Property(property="email", type="string", format="email", example="user1@mail.com"),
     *       @OA\Property(property="phone", type="string", format="phone", example="+77022880808"),
     *       @OA\Property(property="message"),
     *    ),
     * ),
     * @OA\Response(
     *    response=422,
     *    description="Неправильные данные",
     *    @OA\JsonContent(
     *       @OA\Property(property="error", type="string", example="Введенные данные неправильны")
     *        )
     *     ),
     *
     * @OA\Response(
     *    response=200,
     *    description="Успешный ответ",
     *    @OA\JsonContent(
     *       @OA\Property(property="data", type="object", ref="#/components/schemas/Form")
     *        )
     *     )
     *
     * )
     */
    public function form(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' =>   ['required', 'email'],
            'phone' =>   ['required', 'string', new Phone()],
            'message' => ['required', 'string']
        ]);

        if ($validator->fails())
            return response()->json(['error' => $validator->errors()->first()], 422);

        return response()->json(['data' => [
            'email' => $request->email,
            'phone' => $request->phone,
            'message' => $request->message,
        ]], 200);
    }

    /**
     *
     * @OA\Post(
     * path="/api/ip",
     * summary="Ip",
     * description="IP for test task",
     * operationId="ip",
     * tags={"IP address"},
     * @OA\Response(
     *    response=200,
     *    description="Успешный ответ",
     *    @OA\JsonContent(
     *       @OA\Property(property="data", type="string", example="Данные о местоположении IP адреса")
     *        )
     *     ),
     * @OA\Response(
     *    response=500,
     *    description="Ответ с ошибкой",
     *    @OA\JsonContent(
     *       @OA\Property(property="data", type="string", example="Сервис временно работает")
     *        )
     *     )
     * )
     */
    public function ip(Request $request)
    {
        $response = (new IPRequest())->info($request);

        return response()->json(['data' => $response['countryName'] . ', ' .$response['city']], 200);
    }
}
